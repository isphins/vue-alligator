## Iterating Over Items in Vue.js With V-for

> [demo](https://jsfiddle.net/bonbonpa/pd5t15h2/)

```js
data() {
  return {
    messages:  ['hello', 'vue', 'js'],
    shoppingItems: [
      {name: 'apple', price: '10'},
      {name: 'orange', price: '12'}
    ]
  }
}
```

v-for -> ใช้วนลูปดึงค่าใน Array หรือ Object

```html
<ul>
  <li v-for="msg in messages">{{ msg }}</li>
</ul>

<ul>
  <li v-for="item in shoppingItems">
    {{ item.name }} - {{ item.price }}
  </li>
</ul>
```

v-for -> ใช้งานกับ <template> tag

```html
<template v-for="item in shoppingItems">
  <label> {{ item.name }} </label>
  <label> {{ item.price }} </label>
  <button>Buy</button>
</template>
```

v-for -> ใช้กับ Object

```js
data() {
  return {
    objectItems: {
      key1: 'item1',
      key2: 'item 2',
      key3: 'item3'
    }
  }
}
```

```html
<ul>
  <li v-for="item in objectItems">{{ item }}</li>
</ul>
```

v-for -> กับ key, index

```html
<ul>
  <li v-for="(item, key, index) in objectItems">
    {{ item }} - {{ key }} - {{ index }}
  </li>
</ul>
```

v-for -> ใช้งานกับ Range ช่วงตัวเลข

```html
<ul>
  <li v-for="item in 15">{{ item }}</li>
</ul>
```

v-for -> Key การกำหนด key ให้กับ v-for เพื่อให้สามารถ track ค่าของ object โดยใช้ key เมื่อ object มีการ update ข้อมูล

```html
<ul>
  <li v-for="item in shoppingItems" :key="item.name">
    {{ item.name }} - {{ item.price }}
  </li>
</ul>
```

v-for -> Filtering List การกรองค่าใน list ด้วย computed

```html
<ul>
  <li v-for="item in itemsLessThanTen" :key="item.name">
    {{ item.name }} - {{ item.price }}
  </li>
</ul>
```
```js
data() {
  return {
    shoppingItems: [
      {name: 'apple', price: '7'},
      {name: 'orange', price: '12'}
    ]
  }
},
computed:{
  itemsLessThanTen: function() {
    return this.shoppingItems.filter(function(item) {
      return item.price > 10;
    })
  }
}
```

v-for -> Filtering List การกรองค่าใน list ด้วย method

```html
<ul>
  <li v-for="item in filterItems(shoppingItems)" :key="item.name">
    {{ item.name }} - {{ item.price }}
  </li>
</ul>
```
```js
data() {
  return {
    shoppingItems: [
      {name: 'apple', price: '7'},
      {name: 'orange', price: '12'}
    ]
  }
},
methods:{
  filterItems: function(items) {
    return items.filter(function(item) {
      return item.price > 10;
    })
  }
}
```



