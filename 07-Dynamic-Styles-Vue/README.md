# Dynamic Styles With Vue.js

> [demo](https://jsfiddle.net/bonbonpa/8pxwjvar/)

Vue ช่วยให้สามารถกหนด style และ class ที่มีผลกับ elements โดยใช้ directive >> v-bind:style และ v-bind:class 

## Dynamically Binding Styles

หากต้องการเพิ่มขนาดแบบอักษร ตามข้อมูลผู้ใช้ vue สามารถใช้ v-bind:style ได้

```js
data() {
  return {
    fontSize: 10
  }
}
```

เพิ่มปุ่ม 2 ปุ่ม สำหรับเพิ่มลดขนาดอักษร

```html
<button v-on:click="fontSize++">
  Increase font size
</button>
<button v-on:click="fontSize--">
  Decrease font size
</button>

<p v-bind:style="{ fontSize: fontSize + 'px' }">
  Font size is: {{ fontSize }}
</p>
``` 

หลังจากมีปุ่ม 2 ปุ่มแล้วให้ เพิ่ม v-bind:style >> fontSize มาจาก model ใน vue โดยที่ fontSize คือ css font-size

Note: camcelCase จะถูกแปลงเป็น dash-case (ex: fontSize >> font-size)

แทนที่จะมี style แบบ inline เราสามารถเก็บ style ไว้ใน object ใน model เพื่อนำไปใช้ใน v-bind:style directive ได้

### Array of Style

หากจำเป็นเราสามารถเพิ่ม object style ต่างๆได้ ใน v-bind:style directive

```js
baseStyles: {
  fontWeight:'800',
  color: 'red'
},
overrideStyles: {
  color:'blue'
},
```

และใน Template ให้ใส่ arrray ของ style

```html
<p v-bind:style="[baseStyles, overrideStyles]">
  baseStyles and overrideStyles
</p>
```

Note : style ในลำดับหลังจะมีความสำคัญกว่า ดังนั้นจึงถูกแทนที่ด้วย style ลำดับหลัง

### Automatic Prefixing

ใน vue จะเพิ่ม vendor prefix ให้อัตโนมัติ สำหรับคำนำหน้าเช่น -moz, -webkit

## Binding Classes Dynamically

การใช้ style โดยตรงจะซับซ้อนเมื่อ ความต้องการเปลี่ยนแปลง โดยจะใช้ v-bind:style เพื่อช่วยให้สามารถ binding class กับ elements ได้

```js
data() {
  return {
    selected: 'Home',
    menuItems: ['Home', 'About', 'Contact']
  }
}
```

เมื่อ Item ถูกคลิก เราจะเซ็ทค่าของ seleted ให้เท่ากับ item เราจะใช้ v-bind:class directive เพื่อกำหนด class ที่เลือกใน item ถ้าตัวแปรที่เลือกมีค่าเท่ากับรายการปัจจุบัน

```html
<ul>
  <li v-for="item in menuItems"
    v-on:click="selected = item"
    v-bind:class="{ selected: selected == item}">
      {{item}}
  </li>
</ul>
```

### Binding Multiple Classes

เราสามารถใช้ class หลายๆ class ได้โดยส่งผ่าน array ของ class ไปยัง v-bind:class directive

```js
data() {
  return {
    classArray: ['selected', 'underlined']
  }
}
```

และใน template เราสามรถอ้างถึง class ตัวเดียวกันด้วย v-bind:class directive

```html
<p v-bind:class="[classArray]">
  Multiple classes {{ classArray }}
</p>
```

ซึ่งจะได้ class selected และ underline 