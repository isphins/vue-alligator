## Vue.js Templating

> [demo](https://jsfiddle.net/bonbonpa/1qwtfp53/)

```js
data() {
  return {
    text: 'Hello world',
    htmlContent: 'Hello <b>bold</b> new world!',
    isDisabledAttr: true
  }
}
```

### Interpolation -> การแก้ไขเปลี่ยนแปลง

คือ การกระทำการหนึ่งหรือมากกว่า ของตัวแปร ที่ทำให้เกิดการเปลี่ยนแปลงค่า และการแสดงผลใน Template

> Text

การแสดงค่าอย่างง่ายๆด้วย {{ Text }} ปีกกาคู่เป็นการทำ one-way data binding หรือการผูกค่าแบบทางเดียวจาก model ไปยัง Template

> One way binding 

จะผูกค่าที่ได้จากมาจาก model ลงใน view

```html
<span>Text binding: {{ text }}</span><br>
```
> One Time binding -> การผูกค่าแบบครั้งเดียว

สามารถใช้ v-once directive สำหรับการ binding ค่าแบบครั้งเดียวโดยที่จะไม่มีการเปลี่ยนแปลงอีก

เมื่อใช้ v-once หมายถึงหากมีการเปลี่ยนแปลงค่าจาก model จะไม่มีผลกับ ค่าที่อยู่ใน template ของ v-once

```html
<span v-once>Text binding: {{ text }}</span><br>
```
> Raw HTML

การ binding string ที่มี HTML tag เมื่อใช้กับ {{ text }} จะเป็น ออกมาเป็นข้อความธรรมดาหรือ plain string

ถ้าหากต้องการให้ออกมาเป็น HTML tag และต้องมากจากแหล่งที่เชื่อได้เท่านั้น สามารถใช้ v-html diretive เพื่อทำให้ค่าที่ได้ถูก Render เป็น HTML tag

```html
<span v-html="htmlContent"></span>
```

> Attributes

นอกจากค่าแลัวยังสามารถ เพิ่ม attributes ให้กับ element ได้โดยใช้ vue template แทน mustache syntax สามารถใช้ v-bind directive เพื่อผูกค่าจาก model

```html
<button v-bind:disabled="isDisabledAttr">
  Disabled
</button>

<button :disabled="isDisabledAttr">
  Disabled
</button>
```

การผูกค่าด้วย `v-bind:attr=modelValue` สามารถเขียนแบบสั้นๆด้วย `:attr=modelValue`

> JS Expressions

vue สนับสนุน การเปลี่ยนแปลงค่าง่ายๆด้วย javasciprt expression ภายใน data binding

```html
<span>{{ text + ' ' + isDisabledAttr }}</span>
```

vue สามารถใช้ได้แค่ 1 expression ใน {{}} เท่านั้น

### Directive

เช่น v-if,v-for

### Filters

fILters สามารถใช้ร่วมกับค่าใน model เพื่อเปลี่ยนแปลงค่า ในขั้นตอนการแสดงผลใน view

เช่น หากต้องการให้ข้อความแสดงเป็นตัวใหญ่ทั้งหมดสามารถใช้ filters และสร้าง function เพื่อเปลี่ยนแปลงค่า

```js
filers: {
  allCaps: function(value){
    if(!value) return '';
    value = value.toString();
    return value.toUpperCase();
  }
}
```

```html
<span>{{  text | allCaps }}</span>
```



