## Introduction to Computed Properties in Vue.js

> [demo](https://jsfiddle.net/bonbonpa/garL47fv/)

```js
data() {
  return {
    results: [
      	{
          name: 'English',
          marks: 70
        },
        {
          name: 'Math',
          marks: 80
        },
        {
          name: 'History',
          marks: 90
        }
      ]
  }
}
```

### Computed Properties

Computed properties ช่วยให้เราสามารถกำหนดค่าของ model แบบใช้ในการ จัดการกับค่า ด้วยการคำนวณที่มีความซับซ้อนลงใน view ได้

และจะมีการ update ค่าเมื่อมีการเปลี่ยนแปลงตามที่ต้องการเท่านั้น

```js
computed: {
  totalMarks: function(){
    let total = 0;
    for( let i = 0; i < this.results.length; i++){
      total += parseInt(this.results[i].marks);
    }
    return total;
  }
}
```

```html
<div id="app">
  <div v-for="subject in results">
    <input v-model="subject.marks">
    <span>
      Marks for {{ subject.name }}: {{ subject.marks }}
    </span>
  </div>

  <span>
    Total marks are: {{ totalMarks }}
  </span>
</div>
```

> Computed Proerties vs Methods

เราสามารถได้ผลลัพธ์เดียวกันเมื่อใช้ method 

การใช้ method เพื่อให้ผลเดียวกันกับ computed โดยการย้าย totalMarks

```js
 methods: {
    totalMarks2: function(){
      let total = 0;
      for(let i = 0; i < this.results.length; i++){
        total += parseInt(this.results[i].marks);
      }
      return total;
   }
} 
```

การใช้ method จะให้ผลออกมาเหมือนกับ computed การใช้งาน methods จะถูกเรียกทุกๆครั้งที่ page ทำการ render ใหม่ ทุกครั้งที่มีการเปลี่ยนแปลง

แต่ถ้าใช้ computed property ตัว vue จะจำค่าที่ใช้ ขึ้นอยู่กับ computed property โดย vue สามารถคำนวณค่าถ้ามีการเปลี่ยนแปลง หากไม่ใช่ จะคืนค่าที่อยู่ใน cached กลับไป

ด้วยเหตุนี้ ฟังก์ชันจะต้องเป็น pure function ไม่มีผลข้างเคียงใดๆ ผลลัพธ์จะต้องขึ้นอยู่กับค่าที่ส่งเข้ามาในฟังก์ชันเท่านั้น

> Computed Setters

โดยค่าเริ่มต้น ของ computed properties คือ getter แต่ก็สามารถใช้ setter ได้เช่นกัน

```js
computed: {
  fullName: {
    get: function() {
      return this.firstName + this.lastName;
    },
    set: function(value) {
      let names = value.split(' ');
      this.firstName = names[0];
      this.lastName = names[names.length - 1];
    }
  }
}
```

โดยการใช้ getters และ setters เราสามารถผูกค่า อินพุตได้อย่างถูกต้องกับ model ถ้ามีการกำหนด fullName ใน method เมื่อ string ถูกส่งเข้ามาจะถูกแบ่งออกเป็น ชื่อและนามสกุล