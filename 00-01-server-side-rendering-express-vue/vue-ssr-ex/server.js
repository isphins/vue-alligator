const fs = require('fs')
const express = require('express')
const { createBundleRenderer } = require('vue-server-renderer')

const bundleRenderer = createBundleRenderer(
    // Load the SSR bundle with require.
    require('./dist/vue-ssr-bundle.json'),
    {
        template: fs.readFileSync('./index.html', 'utf-8')
    }
);
// Create the express app.
const app = express();

// Serve static assets from ./dist on the /dist route.
app.use('/dist',express.static('dist'));

// Render all other routes with the bundleRenderer.
app.get('*',(req,res) => {
    // Renders directly to the response stream.
    // The argument is passed as "context" to main.server.js in the SSR bundle.
    bundleRenderer.renderToStream({url: req.path}).pipe(res);
});

app.listen(8080);