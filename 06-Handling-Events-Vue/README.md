## Handling Events With Vue.js

> [demo](https://jsfiddle.net/bonbonpa/ps7hp629/)

vue ช่วยให้เราสามารถจัดการกับ event ที่เกิดขึ้นโดยผู้ใช้ได้ การจัดการ event ช่วยเพิ่มการตอบสนองระหว่าง web application กับผู้ใช้งาน vue ใช้ v-on ในการจัดการกับ event เหล่านี้

### V-on

การทำการโต้ตอบกับผู้ใช้กับข้อมูล อาจทำได้โดย การ click หรือ keyup เราสามารถใช้ v-on เพื่อจัดการกับเหตุการณ์เหล่านี้ได้ ตัวอย่าง การนับจำนวนที่เพิ่มขึ้นจากการคลิกที่ปุ่มโดยให้เริ่มต้น จาก 0

```js
data() {
  return {
    count: 0
  }
}
```

```html
<label>Count is: {{count}}</label>
<button v-on:click="count++">Increment</button>
```

v-on จะเรียกใช้ expression หรือ method ที่กำหนดเมื่อมีการคลิก เกิดขึ้นบน ปุ่ม

> Binding methods to v-on -> การผูกค่า methods ไปที่ v-on

เราสามารถผูกค่า methods ไปที่ event โดยใช้ ชื่อได้

```html
<input v-model="addValue">
<button v-on:click="addToCount">Add</button>
```

```js
methods: {
  addToCount: function() {
    this.count = this.count + parseInt(this.addValue);
  }
}
```

> Shorthand สำหรับ v-on

เราสามารถใช้ @ แทนการประกาศ v-on ได้

```html
<button @click="addToCount">Add</button>
```

> Event Modifiers -> การแก้ไข event

มีการเรียกใช้งานหลายๆครั้ง เมื่อต้องการจัดการกับ event โดย vue ทำให้เราใช้งานได้ง่ายขึ้นโดยการใช้ modifiers

เช่น `event.preventDefault()` คือ การที่ไม่ให้ การทำงาน default ของ browser ถูกเรียกใช้งานเมื่อเกิด event

```html
<a href="test" @click.prevent="addToCount">Add</a>
```

> modifiers ที่ใช้ใน Vue.

- stop - ป้องกัน event ในระดับ DOM
- prevent - ป้องกัน ค่าเริ่มต้นของ browser
- capture - capture mode ใช้สำหรับจัดการกับ event
- self - เรียกใช้เมื่อเป้าหมายของ event เป็นตัวเองเท่านั้น
- once - ทำงานเพียงครั้งเดียว

### Key Modifiers

คล้ายกับ event modifiers เราสามารถ เพิ่ม key modifiers เพื่อจัดการกับ event เช่น keyup

```html
<input v-on:keyup.13="addToCount" v-model="addValue">
```

ในตัวอย่าง เมื่อมี event keyup ที่ key code หมายเลข 13 (enter) หรือคือการกดปุ่ม enter จะไปเรียกใช้งาน addToCount method

 เนื่องจากเป็นไปได้ยากที่จะจำ key code ได้ทั้งหมด vue จึงมีการจัดการกำหนด มาให้ เช่น enter, tab, delete, esc, space, left

 นอกจากนี้ยังสามารถตั้งค่า key code ได้เอง

 ```js
 Vue.config.keyCodes.a = 65
 ```