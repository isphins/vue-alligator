## Conditional Directives With Vue.js

> [demo](https://jsfiddle.net/bonbonpa/kxgb27zb/)

```js
new Vue({
            el: '#hello-world-app',
            data () {
                return {
                    msg: 'Hello Alligator Vue! Hey Vue',
                    isLoggedIn: false
                }
            }
        });
```

> v-if

```html
<button v-if="isLoggedIn">Logout</button>
```

v-if -> ใช้ร่วมกับ <template> tag แสดงเมื่อเงื่อนไขเป็นจริง

```html
<template v-if="isLoggedIn">
  <label> Logout </button>
  <button> Logout </button>
</template>
```

v-else -> เมื่อ v-if เป็นเท็จจะทำให้ v-else แสดง Log In

```html
<button v-if="isLoggedIn"> Logout </button>
<button v-else> Log In </button>
```

v-else-if -> เมื่อ isLoginDisabled เป็นจริงจะแสดง ข้อความใน label

```html
<button v-if="isLoggedIn"> Logout </button>
<label v-else-if="isLoginDisabled"> Register disabled </label>
<button v-else> Log In </button>
```

v-show -> จะคล้ายกับ v-if ใช้ในกรณีที่ต้องการซ่อนหรือแสดง DOM

- v-if : จะ Render element ไปที่ DOM เมื่อเงื่อนไขเป็นจริง
- v-show : จะ Render element ทั้งหมดไปที่ DOM แต่จะแสดงหรือซ่อนด้วย CSS display เมื่อเงื่อนไขเป้นจริงหรือเท็จ
- v-show : ไม่สามารถใช้ร่วมกับ v-else, v-else-if ได้